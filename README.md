ToyDroid
======================
A framework for Android apps analysis based on 
[WALA](http://wala.sourceforge.net/wiki/index.php/Main_Page). 
[Here](https://github.com/hjjandy/WALA)
provides a modified version which removes the Eclipse Plugin dependencies.


ToyDroid.Utils
======================
Utility functions of WALA-based static analysis for Android apps.

