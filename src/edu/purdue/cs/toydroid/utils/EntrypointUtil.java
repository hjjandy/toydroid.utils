package edu.purdue.cs.toydroid.utils;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.classLoader.Language;
import com.ibm.wala.dalvik.classLoader.DexIRFactory;
import com.ibm.wala.ipa.callgraph.*;
import com.ibm.wala.ipa.callgraph.AnalysisOptions.ReflectionOptions;
import com.ibm.wala.ipa.callgraph.impl.DefaultEntrypoint;
import com.ibm.wala.ipa.callgraph.impl.Util;
import com.ibm.wala.ipa.callgraph.propagation.SSAPropagationCallGraphBuilder;
import com.ibm.wala.ipa.cha.IClassHierarchy;
import com.ibm.wala.ssa.IR;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.ssa.SSACheckCastInstruction;
import com.ibm.wala.ssa.SSAInstruction;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.Selector;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.MonitorUtil.IProgressMonitor;
import com.ibm.wala.util.NullProgressMonitor;
import com.ibm.wala.util.collections.Pair;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.util.*;

public class EntrypointUtil {
	private static final String activityTypeString = "Landroid/app/Activity";
	private static final String serviceTypeString = "Landroid/app/Service";
	private static final String receiverTypeString = "Landroid/content/BroadcastReceiver";
	private static final String providerTypeString = "Landroid/content/ContentProvider";
	private static final String applicationTypeString = "Landroid/app/Application";
	private static final List<String> componentTypeStrings = Arrays
			.asList(activityTypeString, serviceTypeString, receiverTypeString, providerTypeString,
					applicationTypeString);
	private static Logger logger = LogManager.getLogger(EntrypointUtil.class);
	private static IClassHierarchy classHierarchy;
	private static Set<IMethod> simpleUncalledMethods = new HashSet<IMethod>();
	// sig-><class, entry method>: if entry is in super class, sig = currClass."|".name.selector
	private static Map<String, Pair<IClass, IMethod>> complexUncalledMethods = new HashMap<>();
	private static Set<String> processedClasses = new HashSet<String>();
	private static Set<String> discoveredEntrypoints = new HashSet<String>();
	private static List<Entrypoint> allEntrypoints = new LinkedList<Entrypoint>();
	private static int nEntrypoints = 0;
	private static IClass classActivity, classService, classReceiver, classProvider, classApplication;
	private static String[] preAndroidLibAsPrimordial = {"Landroid/support/", "Landroid/arch/"};
	private static String excludeAndroidLibFile = "dat/AndroidSupportExclude.txt";
	private static List<String> excludedAndroidSupports = new LinkedList<>();

	static {
		File excludeSupport = new File(excludeAndroidLibFile);
		boolean overridenByConfig = false;
		if (excludeSupport.exists()) {
			try {
				BufferedReader reader = new BufferedReader(new FileReader(excludeSupport));
				String line;
				while ((line = reader.readLine()) != null) {
					line = line.trim();
					if (!line.startsWith("#")) {
						excludedAndroidSupports.add(line);
					}
				}
				reader.close();
				overridenByConfig = true;
			} catch (Exception e) {
				e.printStackTrace();
			}
		}
		if (!overridenByConfig) {
			excludedAndroidSupports.addAll(Arrays.asList(preAndroidLibAsPrimordial));
		}
	}

	private static boolean excludeAndroidLib(String clazz) {
		for (String str : excludedAndroidSupports) {
			if (clazz.startsWith(str)) {
				return true;
			}
		}
		return false;
	}

	public static void addEntrypoint(Entrypoint ep) {
		allEntrypoints.add(ep);
		nEntrypoints++;
		logger.debug("Discovered entrypoint: {}::{}", ep.getMethod().getDeclaringClass().getName().toString(),
					 ep.getMethod().getName().toString());
	}

	public static void addEntrypoint(Entrypoint ep, IClass klass) {
		allEntrypoints.add(ep);
		nEntrypoints++;
		logger.debug("Discovered entrypoint: {}::{}", klass.getName().toString(), ep.getMethod().getName().toString());
	}

	/**
	 * Every time remove an entrypoint from records. After all are iterated, no
	 * more entries can be retrieved.
	 *
	 * @return
	 */
	public static Entrypoint nextEntrypoint() {
		if (allEntrypoints.isEmpty())
			return null;
		return allEntrypoints.remove(0);
	}

	public static int allEntrypointCount() {
		return nEntrypoints;
	}

	public static void initialEntrypoints(IClassHierarchy ich, Set<String> compClasses) {
		classHierarchy = ich;
		for (IClass k : ich) {
			if (k.getClassLoader().getReference().equals(ClassLoaderReference.Application)) {
				String kName = k.getName().toString();
				if (compClasses.contains(kName)) {
					initialEntrypoints(ich, k);
				}
			}
		}
		logger.info("Initial entrypoints: {}", allEntrypoints.size());
	}

	private static void initialEntrypoints(IClassHierarchy ich, IClass clazz) {
		IClass comp = isAndroidComponent(clazz);
		// it must be an Android component defined in AndroidManifest.xml
		if (comp == null) {
			// some apps wrap their own components (e.g., Activity) instead of extending Activity.
			// for example: com.baidu.searchbox.reader(APK) inside com.baidu.searchbox(APK)
			String kName = clazz.getName().toString();
			if (ResourceUtil.isActivity(kName)) {
				if (classActivity == null) {
					classActivity = ich
							.lookupClass(TypeReference.find(ClassLoaderReference.Primordial, activityTypeString));
				}
				comp = classActivity;
			} else if (ResourceUtil.isService(kName)) {
				if (classService == null) {
					classService = ich
							.lookupClass(TypeReference.find(ClassLoaderReference.Primordial, serviceTypeString));
				}
				comp = classService;
			} else if (ResourceUtil.isReceiver(kName)) {
				if (classReceiver == null) {
					classReceiver = ich
							.lookupClass(TypeReference.find(ClassLoaderReference.Primordial, receiverTypeString));
				}
				comp = classReceiver;
			} else if (ResourceUtil.isProvider(kName)) {
				if (classProvider == null) {
					classProvider = ich
							.lookupClass(TypeReference.find(ClassLoaderReference.Primordial, providerTypeString));
				}
				comp = classProvider;
			} else if (ResourceUtil.isApplication(kName)) {
				if (classApplication == null) {
					classApplication = ich
							.lookupClass(TypeReference.find(ClassLoaderReference.Primordial, applicationTypeString));
				}
				comp = classApplication;
			} else {
				logger.warn("Unknown Android Component: {}", kName);
			}
		}
		if (comp != null) {
			Collection<? extends IMethod> methods = clazz.getDeclaredMethods();
			for (IMethod method : methods) {
				String sig = method.getSignature();
				if (method.isPrivate() || discoveredEntrypoints.contains(sig)) {
					continue;
				}
				String mName = method.getName().toString();
				if (mName.startsWith("on") && overridingFramework(comp, method)) {
					discoveredEntrypoints.add(sig);
					addEntrypoint(new DexEntryPointWithInit(method, null, ich));
				}
			}
		}
	}

	public static void discoverNewEntrypoints(AnalysisScope scope) {
		AnalysisCache cache = new AnalysisCacheImpl(new DexIRFactory());
		List<Entrypoint> epList = new LinkedList<Entrypoint>();
		IProgressMonitor pm = new NullProgressMonitor();
		epList.addAll(allEntrypoints);
		Set<IMethod> cachedUncalled = new HashSet<IMethod>();
		Set<IMethod> newUncalled = new HashSet<IMethod>();
		Set<String> mCachedUncalled = new HashSet<>();
		Map<String, Pair<IClass, IMethod>> mNewUncalled = new HashMap<>();
		Set<IMethod> missedMethods = new HashSet<>();
		Set<IMethod> cachedMisshedMethods = new HashSet<>();
		Set<IMethod> newMissed = new HashSet<>();
		int round = 1;

		while (!epList.isEmpty()) {
			logger.info("Entrypoint discovery round-{}: {} entrypoints.", round++, epList.size());
			int origNUncalled = simpleUncalledMethods.size() + complexUncalledMethods.size() + missedMethods.size();
			int origComplexUncalled = complexUncalledMethods.size();
			int origSimpleUncalled = simpleUncalledMethods.size();
			int origMissed = missedMethods.size();
			AnalysisOptions options = new AnalysisOptions(scope, epList);
			options.setReflectionOptions(ReflectionOptions.NONE);
			SSAPropagationCallGraphBuilder cgBuilder = Util.makeZeroCFABuilder(Language.JAVA, options, cache, classHierarchy, scope);
			try {
				CallGraph cg = cgBuilder.makeCallGraph(options, pm);
				for (CGNode n : cg) {
					IMethod m = n.getMethod();
					if (cg.getEntrypointNodes().contains(m)) {
						continue;
					}
					String mName = m.getName().toString();
					if (m.isPrivate() || m.isSynthetic()) {
						continue;
					} else if ("<init>".equals(mName)) {
						processClass(m.getDeclaringClass());
					}
				}

				for (CGNode n : cg) {
					IMethod m = n.getMethod();
					String mName = m.getName().toString();
					if (m.isPrivate() || m.isSynthetic() || cg.getEntrypointNodes().contains(n) ||
						"<init>".equals(mName)) {
						continue;
					}
					if (simpleUncalledMethods.contains(m)) {
						simpleUncalledMethods.remove(m);
					}
				}

				// resolve some methods that are invoked but not present in CG (use all possible targets if abstract method is invoked)
				for (CGNode n : cg) {
					if (!n.equals(cg.getFakeRootNode())) {
						IR ir = n.getIR();
						if (ir == null)
							continue;
						for (SSAInstruction inst : ir.getInstructions()) {
							if (inst instanceof SSACheckCastInstruction) {
								SSACheckCastInstruction checkCast = (SSACheckCastInstruction) inst;
								TypeReference[] types = checkCast.getDeclaredResultTypes();
								if (types != null && types.length > 0) {
									for (int i = 0; i < types.length; i++) {
										IClass resultClass = classHierarchy.lookupClass(types[i]);
										if (resultClass != null) {
											processClass(resultClass);
										}
									}
								}
							} else if (inst instanceof SSAAbstractInvokeInstruction) {
								SSAAbstractInvokeInstruction invoke = (SSAAbstractInvokeInstruction) inst;
								MethodReference mRef = invoke.getDeclaredTarget();
								int nTarget = cg.getNumberOfTargets(n, invoke.getCallSite());
								if (mRef == null || nTarget > 0)
									continue;
								if (nTarget == 0) {
									IMethod missedMethod = classHierarchy.resolveMethod(mRef);
									if (missedMethod == null || missedMethod.isNative()) {
										continue;
									}
									if (!missedMethod.isAbstract() &&
										missedMethod.getDeclaringClass().getClassLoader().getReference()
													.equals(ClassLoaderReference.Application) &&
										!excludeAndroidLib(mRef.getDeclaringClass().getName().toString())) {
										missedMethods.add(missedMethod);
										//logger.info("MISSED: {}", missedMethod);
									} else if (missedMethod.isAbstract()) {
										IClass k = missedMethod.getDeclaringClass();
										Selector selector = missedMethod.getSelector();
										if (k.isInterface()) {
											Set<IClass> allImp = classHierarchy.getImplementors(k.getReference());
											if (allImp != null && !allImp.isEmpty()) {
												for (IClass imp : allImp) {
													IMethod impMethod = imp.getMethod(selector);
													if (impMethod != null && !impMethod.isAbstract() &&
														impMethod.getDeclaringClass().getClassLoader().getReference()
																 .equals(ClassLoaderReference.Application) &&
														!excludeAndroidLib(imp.getName().toString())) {
														missedMethods.add(impMethod);
														//logger.info("Missed-Imp: {}", imp.getMethod(selector));
													}
												}
											}
										} else if (k.isAbstract()) {
											Collection<IClass> allSub = classHierarchy
													.computeSubClasses(k.getReference());
											if (allSub != null && !allSub.isEmpty()) {
												for (IClass sub : allSub) {
													IMethod subMethod = sub.getMethod(selector);
													if (subMethod != null && !subMethod.isAbstract() &&
														subMethod.getDeclaringClass().getClassLoader().getReference()
																 .equals(ClassLoaderReference.Application) &&
														!excludeAndroidLib(sub.getName().toString())) {
														missedMethods.add(subMethod);
														//logger.info("Missed-Sub: {}", sub.getMethod(selector));
													}
												}
											}
										}
									}
								}
							}
						}
					}
				}
			} catch (IllegalArgumentException e) {
				e.printStackTrace();
			} catch (CallGraphBuilderCancelException e) {
				e.printStackTrace();
			}

			if (origNUncalled == simpleUncalledMethods.size() + complexUncalledMethods.size() + missedMethods.size()) {
				break;
			}
			if (origSimpleUncalled < simpleUncalledMethods.size()) {
				newUncalled.addAll(simpleUncalledMethods);
				newUncalled.removeAll(cachedUncalled);
				cachedUncalled.addAll(simpleUncalledMethods);
			}
			if (origComplexUncalled < complexUncalledMethods.size()) {
				mNewUncalled.putAll(complexUncalledMethods);
				for (String key : mCachedUncalled) {
					mNewUncalled.remove(key);
				}
				mCachedUncalled.addAll(mNewUncalled.keySet());
			}
			if (origMissed < missedMethods.size()) {
				//logger.info("#Missed Methods: {}", missedMethods.size());
				newMissed.addAll(missedMethods);
				newMissed.removeAll(cachedMisshedMethods);
				cachedMisshedMethods.addAll(missedMethods);
			}

			// only build a big CG for newly discovered entries. this way may
			// discover fewer new entries than building CG with all discovered
			// entries as a whole
			epList.clear();
			for (IMethod m : newUncalled) {
				epList.add(new DexEntryPointWithInit(m, null, classHierarchy));
			}
			Set<Map.Entry<String, Pair<IClass, IMethod>>> complexUncalled = mNewUncalled.entrySet();
			for (Map.Entry<String, Pair<IClass, IMethod>> entry : complexUncalled) {
				String sig = entry.getKey();
				Pair<IClass, IMethod> target = entry.getValue();
				IClass targetClass = target.fst;
				IMethod targetMethod = target.snd;
				epList.add(new DexEntryPointWithInit(targetMethod, targetClass, classHierarchy));
			}
			for (IMethod m : newMissed) {
				epList.add(new DexEntryPointWithInit(m, null, classHierarchy));
				//logger.warn("MissedEntry: {}", m);
			}
			newUncalled.clear();
			mNewUncalled.clear();
			newMissed.clear();
		}
		cachedUncalled.clear();
		cachedUncalled = null;
		epList = null;
		newUncalled = null;
		mNewUncalled = null;
		mCachedUncalled = null;
		for (IMethod m : simpleUncalledMethods) {
			discoveredEntrypoints.add(m.getSignature());
			addEntrypoint(new DexEntryPointWithInit(m, null, classHierarchy));
		}
		Set<Map.Entry<String, Pair<IClass, IMethod>>> complexUncalled = complexUncalledMethods.entrySet();
		for (Map.Entry<String, Pair<IClass, IMethod>> entry : complexUncalled) {
			Pair<IClass, IMethod> target = entry.getValue();
			IClass targetClass = target.fst;
			IMethod targetMethod = target.snd;
			discoveredEntrypoints.add(entry.getKey());
			addEntrypoint(new DexEntryPointWithInit(targetMethod, targetClass, classHierarchy), targetClass);
		}
		logger.info("Newly discovered entrypoints: {}", simpleUncalledMethods.size() + complexUncalledMethods.size());
		simpleUncalledMethods.clear();
		complexUncalledMethods.clear();
	}

	private static void processClass(IClass clazz) {
		if (clazz.isAbstract() || clazz.isArrayClass() ||
			!clazz.getClassLoader().getReference().equals(ClassLoaderReference.Application)) {
			return;
		}
		String cName = clazz.getName().toString();
		if (excludeAndroidLib(cName)) {
			return;
		}
		if (processedClasses.contains(cName)) {
			return;
		}
		processedClasses.add(cName);
		//logger.info("ProcessedClass: {}", cName);

		Set<IClass> primordialClasses = new HashSet<IClass>();
		Set<IClass> nonPrimordialClasses = new HashSet<>();
		visitAllParents(clazz, primordialClasses, nonPrimordialClasses);
		Set<String> checkedMethods = new HashSet<>();
		for (IMethod m : clazz.getDeclaredMethods()) {
			String sig = m.getSignature();
			String name = m.getName().toString();
			String selector = m.getSelector().toString();
			checkedMethods.add(name + selector);
			if (m.isAbstract() || m.isNative() || m.isStatic() || m.isPrivate() || m.isSynthetic() ||
				m.getName().toString().equals("<init>") || discoveredEntrypoints.contains(sig)) {
				continue;
			}
			if (overridingFramework(primordialClasses, m)) {
				simpleUncalledMethods.add(m);
			}
		}
		for (IMethod m : clazz.getAllMethods()) {
			IClass k = m.getDeclaringClass();
			if (!nonPrimordialClasses.contains(k)) {
				continue;
			}
			String sig = m.getSignature();
			if (m.isAbstract() || m.isNative() || m.isStatic() || m.isPrivate() || m.isSynthetic() ||
				m.getName().toString().equals("<init>")) {
				continue;
			}
			String name = m.getName().toString();
			String selector = m.getSelector().toString();
			String halfSig = name + selector;
			if (checkedMethods.contains(halfSig)) {
				continue;
			}
			checkedMethods.add(halfSig);
			if (overridingFramework(primordialClasses, m)) {
				String fullSig = cName + "|" + name + selector;
				complexUncalledMethods.put(fullSig, Pair.make(clazz, m));
			}
		}
	}

	private static IClass isAndroidComponent(IClass clazz) {
		IClass comp = null;
		boolean found = false;
		IClass s = clazz.getSuperclass();
		while (s != null) {
			if (comp == null && s.getClassLoader().getReference().equals(ClassLoaderReference.Primordial)) {
				comp = s;
			}
			String sName = s.getName().toString();
			if (componentTypeStrings.contains(sName)) {
				found = true;
				break;
			}
			s = s.getSuperclass();
		}
		if (!found)
			comp = null;
		return comp;
	}

	/**
	 * @param clazz
	 * @param primordialParents    - return values
	 * @param nonPrimordialParents - return values
	 */
	private static void visitAllParents(IClass clazz, Set<IClass> primordialParents, Set<IClass> nonPrimordialParents) {
		IClass s = clazz.getSuperclass();
		while (s != null) {
			if (s.getName().toString().equals("Ljava/lang/Object")) {
				break;
			}
			if (s.getClassLoader().getReference().equals(ClassLoaderReference.Primordial) ||
				excludeAndroidLib(s.getName().toString())) {
				primordialParents.add(s);
				break;
			} else {
				nonPrimordialParents.add(s);
				s = s.getSuperclass();
			}
		}
		Collection<IClass> ifs = clazz.getAllImplementedInterfaces();
		for (IClass k : ifs) {
			if (k.getClassLoader().getReference().equals(ClassLoaderReference.Primordial) ||
				excludeAndroidLib(k.getName().toString())) {
				if (!k.getName().toString().equals("Ljava/lang/Object")) {
					primordialParents.add(k);
				}
			} else {
				visitAllParents(k, primordialParents, nonPrimordialParents);
			}
		}
	}

	private static boolean overridingFramework(Set<IClass> primordialList, IMethod method) {
		for (IClass k : primordialList) {
			if (overridingFramework(k, method)) {
				return true;
			}
		}
		return false;
	}

	private static boolean overridingFramework(IClass compClass, IMethod method) {
		Selector s = method.getSelector();
		IMethod m = compClass.getMethod(s);
		boolean overriding = false;
		if (m != null && !m.getDeclaringClass().getName().toString().equals("Ljava/lang/Object")) {
			overriding = true;
		}
		return overriding;
	}

	private static Collection<IMethod> getAllInitWithParam(IClass klass) {
		ArrayList<IMethod> result = new ArrayList<IMethod>();
		for (IMethod m : klass.getDeclaredMethods()) {
			if (!m.getName().toString().equals("<init>") || m.getNumberOfParameters() <= 1)
				continue;
			// now we got an <init> with parameter(s)
			result.add(m);
		}
		return result;
	}

	public static Entrypoint findInitWithMostParams(Entrypoint ep, IClassHierarchy cha, IClass declaringClass) {
		IMethod bestInit = null;
		int longestLength = 0;
		IClass dxc = declaringClass;
		if (dxc == null)
			dxc = ep.getMethod().getDeclaringClass();
		Collection<IMethod> inits = getAllInitWithParam(dxc);
		for (IMethod m : inits) {
			if (m.getNumberOfParameters() > longestLength) {
				bestInit = m;
				longestLength = m.getNumberOfParameters();
			}
		}
		if (bestInit != null)
			return new DefaultEntrypoint(bestInit, cha);
		else
			return null;
	}

}
