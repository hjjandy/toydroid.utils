package edu.purdue.cs.toydroid.utils;

import com.ibm.wala.classLoader.IClass;
import com.ibm.wala.classLoader.IMethod;
import com.ibm.wala.ipa.cha.ClassHierarchy;
import com.ibm.wala.ipa.slicer.*;
import com.ibm.wala.ipa.slicer.HeapStatement.HeapParamCaller;
import com.ibm.wala.ipa.slicer.HeapStatement.HeapReturnCaller;
import com.ibm.wala.ipa.slicer.Statement.Kind;
import com.ibm.wala.ssa.SSAAbstractInvokeInstruction;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.types.MethodReference;
import com.ibm.wala.types.TypeReference;
import com.ibm.wala.util.WalaException;
import com.ibm.wala.util.strings.StringStuff;
import com.ibm.wala.viz.NodeDecorator;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class WalaUtil {
	private static Logger mLogger = LogManager.getLogger(WalaUtil.class);

	private static ClassHierarchy mCha;

	public static void setClassHierarchy(ClassHierarchy ch) {
		mCha = ch;
	}

	public static String getSignature(SSAAbstractInvokeInstruction instr) {
		MethodReference mRef = instr.getDeclaredTarget();
		return getSignature(mRef);
	}

	public static String getSignature(MethodReference mRef) {
		if (mCha == null) {
			mLogger.warn("Without proper ClassHierarchy set, the signature might be incorrect.");
			return mRef.getSignature();
		}
		IMethod m = mCha.resolveMethod(mRef);
		if (m != null) {
			return m.getSignature();
		}
		return mRef.getSignature();
	}

	public static String stringForStmt(Statement n) {
		Kind k = n.getKind();
		StringBuilder builder = new StringBuilder(k.toString());
		builder.append("\n");
		builder.append(n.getNode());
		builder.append("\n");
		switch (k) {
			case NORMAL:
				NormalStatement ns = (NormalStatement) n;
				builder.append(ns.getInstruction().toString());
				break;
			case PARAM_CALLER:
				ParamCaller pcr = (ParamCaller) n;
				builder.append(pcr.getInstruction().toString());
				builder.append("\n v" + pcr.getValueNumber());
				break;
			case PARAM_CALLEE:
				ParamCallee pce = (ParamCallee) n;
				builder.append("v" + pce.getValueNumber());
				break;
			case NORMAL_RET_CALLER:
				NormalReturnCaller nrcr = (NormalReturnCaller) n;
				builder.append(nrcr.getInstruction().toString());
				break;
			case NORMAL_RET_CALLEE:
				break;
			case HEAP_PARAM_CALLER:
				HeapParamCaller hpcr = (HeapParamCaller) n;
				builder.append(hpcr.getCall().toString());
				builder.append("\n");
				builder.append(hpcr.getLocation().hashCode());
				break;
			case HEAP_RET_CALLER:
				HeapReturnCaller hrcr = (HeapReturnCaller) n;
				builder.append(hrcr.getCall().toString());
				builder.append("\n");
				builder.append(hrcr.getLocation().hashCode());
				break;
			case HEAP_PARAM_CALLEE:
			case HEAP_RET_CALLEE:
				HeapStatement hse = (HeapStatement) n;
				builder.append(hse.getLocation().hashCode());
				break;
			case PHI:
				PhiStatement phiStmt = (PhiStatement) n;
				builder.append(phiStmt.getPhi().toString());
				break;
			default:
				builder.append("\n Unsupported");
				break;
		}
		return builder.toString();
	}

	public static NodeDecorator<Statement> makeNodeDecorator() {
		return new NodeDecorator<Statement>() {

			@Override
			public String getLabel(Statement n) throws WalaException {
				return stringForStmt(n);
			}

		};
	}

	/**
	 * @param name Dot-shaped class name, e.g., java.lang.Object.
	 * @return
	 */
	public static String getPrimordialSuperclassOf(String name) {
		String typeName = StringStuff.deployment2CanonicalTypeString(name);
		IClass klass = mCha.lookupClass(TypeReference.findOrCreate(ClassLoaderReference.Application, typeName));
		IClass superClass = null;
		while (klass != null) {
			superClass = klass.getSuperclass();
			if (superClass != null && superClass.getClassLoader().getReference().equals(ClassLoaderReference.Primordial)) {
				break;
			}
			klass = superClass;
		}
		return superClass == null ? "" : StringStuff.slashToDot(superClass.getName().toString()).substring(1);// 'L'
	}
}
