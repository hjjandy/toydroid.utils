package edu.purdue.cs.toydroid.utils;

import com.ibm.wala.dalvik.classLoader.DexFileModule;
import com.ibm.wala.dalvik.util.AndroidAnalysisScope;
import com.ibm.wala.ipa.callgraph.AnalysisScope;
import com.ibm.wala.types.ClassLoaderReference;
import com.ibm.wala.util.config.AnalysisScopeReader;
import com.ibm.wala.util.io.TemporaryFile;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.jar.JarFile;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

public class AnalysisScopeUtil {

	private static final Logger logger = LogManager.getLogger(AnalysisScopeUtil.class);

	private static final ClassLoader WALA_CLASSLOADER = AnalysisScopeReader.class.getClassLoader();

	private static final String BASIC_FILE = "primordial.txt";

	private static final String EXCLUSIONS = "AndroidRegressionExclusions.txt";

	private static final String PrimordialTag = "Primordial";
	private static final String ApplicationTag = "Application";

	private static List<String> libraryAPKs;
	private static int tempFileIdx;

	public static AnalysisScope makeAnalysisScope(String apkFile)
			throws IOException, FileNotFoundException {
		AnalysisScope scope = null;

		String android_jar = SimpleConfig.getAndroidJar();
		String exclusion_file = SimpleConfig.getExclusionFile();
		String additional_jar;

		logger.info("AndroidJar: {}", android_jar);
		if (exclusion_file == null) {
			exclusion_file = EXCLUSIONS;
		}
		logger.info("ExclusionFile: {}", exclusion_file);

		scope = AnalysisScopeReader.readJavaScope(BASIC_FILE, new File(
				exclusion_file), WALA_CLASSLOADER);

//		AndroidAnalysisScope.addClassPathToScope(android_jar, scope,
//				ClassLoaderReference.Primordial);
		scope.addToScope(ClassLoaderReference.Primordial, new JarFile(android_jar));

		Iterator<String> iter = SimpleConfig.iteratorAdditionalJars();
		while (iter.hasNext()) {
			additional_jar = iter.next();
			int idx = additional_jar.indexOf(',');
			if (idx > 0) {
				String ref = additional_jar.substring(0, idx);
				String path = additional_jar.substring(idx + 1);
				ClassLoaderReference clRef = null;
				logger.info("AdditionalJar[{}]: {}", ref, path);
				if (ref.startsWith(PrimordialTag)) {
					clRef = scope.getPrimordialLoader();
				} else if (ref.startsWith(ApplicationTag)) {
					clRef = scope.getApplicationLoader();
				} else {
					logger.warn("Unrecognized ADDITIONAL_JARS in Config.properties.");
				}
				if (clRef != null) {
					AndroidAnalysisScope.addClassPathToScope(path, scope, clRef);
				}
			}
		}

		scope.setLoaderImpl(ClassLoaderReference.Application,
							"com.ibm.wala.dalvik.classLoader.WDexClassLoaderImpl");

		tempFileIdx = 0;
		int apkIdx = 0;
		libraryAPKs = new LinkedList<String>();
		// Temporarily put main APK in the list and remove it later.
		libraryAPKs.add(apkFile);
		while (apkIdx < libraryAPKs.size()) {
			apkFile = libraryAPKs.get(apkIdx);
			scope.addToScope(ClassLoaderReference.Application,
							 DexFileModule.make(new File(apkFile)));
			multiDexAndLibraryAPKs(scope, apkFile);
			apkIdx++;
		}
		libraryAPKs.remove(0);
		if (libraryAPKs.isEmpty()) {
			libraryAPKs = null;
		}

		return scope;
	}

	private static void multiDexAndLibraryAPKs(AnalysisScope scope, String apkFile) {
		int len = apkFile.length();
		if (len < 5 || !apkFile.substring(len - 4).toLowerCase().equals(".apk")) {
			return;
		}

		ZipFile apkAchive = null;
		ZipEntry entry;
		String entryName;

		try {
			apkAchive = new ZipFile(apkFile);
			Enumeration<?> enums = apkAchive.entries();
			while (enums.hasMoreElements()) {
				entry = (ZipEntry) enums.nextElement();
				entryName = entry.getName();
				if (!entryName.equals("classes.dex") && entryName.startsWith("classes") && entryName.endsWith(".dex")) {
					logger.info("Multi-DEX found: {}", entryName);
					File tf = new File(System.getProperty("java.io.tmpdir") + File.separator + tempFileIdx + "-" + entryName);
					tf.deleteOnExit();
					TemporaryFile.streamToFile(tf, new InputStream[]{apkAchive.getInputStream(entry)});
					scope.addToScope(ClassLoaderReference.Application, DexFileModule.make(tf));
					tempFileIdx++;
				} /*else if (entryName.startsWith("assets/") && entryName.substring(entryName.length() - 4).toLowerCase().equals(".apk")) {
					logger.info("Library APK found: {}", entryName);
					String ename = entryName;
					int idxSlash = ename.lastIndexOf('/');
					if (idxSlash >= 0) {
						ename = ename.substring(idxSlash + 1);
					}
					File tf = new File(System.getProperty("java.io.tmpdir") + File.separator + tempFileIdx + "-" + ename);
					tf.deleteOnExit();
					TemporaryFile.streamToFile(tf, new InputStream[]{apkAchive.getInputStream(entry)});
					if (libraryAPKs == null) {
						libraryAPKs = new LinkedList<String>();
					}
					libraryAPKs.add(tf.getAbsolutePath());
					tempFileIdx++;
				}*/
			}
		} catch (IOException e) {
			e.printStackTrace();
		} finally {
			if (apkAchive != null) {
				try {
					apkAchive.close();
				} catch (Exception e) {

				}
			}
		}
	}

	public static List<String> getLibraryAPKs() {
		return libraryAPKs;
	}
}
